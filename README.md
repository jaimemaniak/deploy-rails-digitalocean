# Deployment guideline using DigitalOcean and Rails 4.2.6

Base on  https://www.digitalocean.com/community/tutorials/deploying-a-rails-app-on-ubuntu-14-04-with-capistrano-nginx-and-puma

## Prerequisites
* Ubuntu 14.04 x64 Droplet(DigitalOcean)
* rails app example in order to deploy into the DigitalOcean server
* ssh key

Take in count every `<SOME_VALUE>` you find will be replaced by the necessary value, for example
`ssh root@<SERVER_IP_ADDRESS>` turns into `ssh root@123.123.123.123`

## List of content
* Creating non-root user named deploy with sudo privileges
* Installing Nginx
* Installing Databases
* Installing RVM and Ruby
* Installing Rails and Bundler
* Setting up SSH Keys
* Adding Deployment Configurations in the Rails App
* Deploying your Rails Application
* After first deploy
* Integration with [semaphore](https://semaphoreci.com/docs/adding-github-bitbucket-project-to-semaphore.html)

### Creating non-root user named deploy with sudo privileges

1. Login as root member using ssh
```
  local$ ssh root@<SERVER_IP_ADDRESS>
```
2. Once your are loged on the server then create a new user called **deploy**
```
  # adduser deploy
```
3. Get root privileges to the new user
```
  # gpasswd -a deploy sudo
```
4. Use the ssh on the server(copy the id_rsa.pub)
```
  # su - deploy
  $ mkdir .ssh
  $ chmod 700 .ssh
  $ vim .ssh/authorized_keys
```
copy the public key into the authorized_keys file and save it after that execute the command
```
  $ chmod 600 .ssh/authorized_keys
```
5. Now you should be able to access as deploy user to the server using ssh
```
  local$ ssh deploy@<SERVER_IP_ADDRESS>
```

### Installing Nginx
```
  deploy@<SERVER_IP_ADDRESS>:~$ sudo apt-get update
  deploy@<SERVER_IP_ADDRESS>:~$ sudo apt-get install curl git-core nginx -y
```
### Installing Databases

You can check the documentation for each db provider in the following links:

* [MySQL](https://www.digitalocean.com/community/tutorials/a-basic-mysql-tutorial)
* [PostgreSQL](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-14-04)
* [MongoDB](https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-12-04)

### Installing RVM and Ruby
```
  deploy@<SERVER_IP_ADDRESS>:~$ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
  deploy@<SERVER_IP_ADDRESS>:~$ curl -sSL https://get.rvm.io | bash -s stable
  deploy@<SERVER_IP_ADDRESS>:~$ source ~/.rvm/scripts/rvm
  deploy@<SERVER_IP_ADDRESS>:~$ rvm requirements
  deploy@<SERVER_IP_ADDRESS>:~$ rvm install 2.2.1
  deploy@<SERVER_IP_ADDRESS>:~$ rvm use 2.2.1 --default
```

### Installing Rails and Bundler
```
  deploy@<SERVER_IP_ADDRESS>:~$ gem install rails -V --no-ri --no-rdoc
  deploy@<SERVER_IP_ADDRESS>:~$ gem install bundler -V --no-ri --no-rdoc
```
you can also install an specific rails version
```
  deploy@<SERVER_IP_ADDRESS>:~$ gem install rails -v '4.2.0' -V --no-ri --no-rdoc
```

### Setting up SSH Keys
```
  local$ cat ~/.ssh/id_rsa.pub | ssh -p 22 deploy@<SERVER_IP_ADDRESS>'cat >> ~/.ssh/authorized_keys'
```

### Adding Deployment Configurations in the Rails App
1. Install Capistrano gems adding them into the Gemfile of your rails project
```
  group :development do
    gem 'capistrano',         require: false
    gem 'capistrano-rvm',     require: false
    gem 'capistrano-rails',   require: false
    gem 'capistrano-bundler', require: false
    gem 'capistrano3-puma',   require: false
  end

  gem 'puma'
```
2. Run bundle
```
  local$ bundle install
```
2. Initialize and configure Capistrano
```
  local$ cap install
```
this step is going to create 3 files, let's take 'Capile' and we are going to
replace the content with:
```
  # Load DSL and Setup Up Stages
  require 'capistrano/setup'
  require 'capistrano/deploy'

  require 'capistrano/rails'
  require 'capistrano/bundler'
  require 'capistrano/rvm'
  require 'capistrano/puma'

  # Loads custom tasks from `lib/capistrano/tasks' if you have any defined.
  Dir.glob('lib/capistrano/tasks/*.rake').each { |r| import r }
```
Replace the contents of 'config/deploy.rb' with the following:
```
  # Change these
  server '<SERVER_IP_ADDRESS>', port: 22, roles: [:web, :app, :db], primary: true

  set :repo_url,        '<GIT_URL_REPO>'
  set :application,     '<APPNAME>'
  set :user,            'deploy'
  set :puma_threads,    [4, 16]
  set :puma_workers,    0

  # Don't change these unless you know what you're doing
  set :pty,             true
  set :use_sudo,        false
  set :stage,           :production
  set :deploy_via,      :remote_cache
  set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
  set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
  set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
  set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
  set :puma_access_log, "#{release_path}/log/puma.error.log"
  set :puma_error_log,  "#{release_path}/log/puma.access.log"
  set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
  set :puma_preload_app, true
  set :puma_worker_timeout, nil
  set :puma_init_active_record, true  # Change to false when not using ActiveRecord

  ## Defaults:
  # set :scm,           :git
  # set :branch,        :master
  # set :format,        :pretty
  # set :log_level,     :debug
  # set :keep_releases, 5

  ## Linked Files & Directories (Default None):
  # set :linked_files, %w{config/database.yml}
  # set :linked_dirs,  %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

  namespace :puma do
    desc 'Create Directories for Puma Pids and Socket'
    task :make_dirs do
      on roles(:app) do
        execute "mkdir #{shared_path}/tmp/sockets -p"
        execute "mkdir #{shared_path}/tmp/pids -p"
      end
    end

    before :start, :make_dirs
  end

  namespace :deploy do
    desc "Make sure local git is in sync with remote."
    task :check_revision do
      on roles(:app) do
        unless `git rev-parse HEAD` == `git rev-parse origin/master`
          puts "WARNING: HEAD is not the same as origin/master"
          puts "Run `git push` to sync changes."
          exit
        end
      end
    end

    desc 'Initial Deploy'
    task :initial do
      on roles(:app) do
        before 'deploy:restart', 'puma:start'
        invoke 'deploy'
      end
    end

    desc 'Restart application'
    task :restart do
      on roles(:app), in: :sequence, wait: 5 do
        invoke 'puma:restart'
      end
    end

    before :starting,     :check_revision
    after  :finishing,    :compile_assets
    after  :finishing,    :cleanup
    after  :finishing,    :restart
  end

  # ps aux | grep puma    # Get puma pid
  # kill -s SIGUSR2 pid   # Restart puma
  # kill -s SIGTERM pid   # Stop puma
```
Now, Nginx needs to be configured. Create 'config/nginx.conf' in your Rails project directory,
and add the following to it (again, replacing with your parameters):
```
upstream puma {
  server unix:///home/deploy/apps/<APPNAME>/shared/tmp/sockets/<APPNAME>-puma.sock;
}

server {
  listen 80 default_server deferred;
  # server_name example.com;

  root /home/deploy/apps/<APPNAME>/current/public;
  access_log /home/deploy/apps/<APPNAME>/current/log/nginx.access.log;
  error_log /home/deploy/apps/<APPNAME>/current/log/nginx.error.log info;

  location ^~ /assets/ {
    gzip_static on;
    expires max;
    add_header Cache-Control public;
  }

  try_files $uri/index.html $uri @puma;
  location @puma {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;

    proxy_pass http://puma;
  }

  error_page 500 502 503 504 /500.html;
  client_max_body_size 10M;
  keepalive_timeout 10;
}
```

### Deploying your Rails Application
Make a commit with all the changes about your rails project and push them
```
  local$ git add -A
  local$ git commit -m "Set up Puma, Nginx & Capistrano"
  local$ git push origin master
```
then MAKE YOUR FIRST DEPLOY
```
cap production deploy:initial
```
This may take anywhere between 5-15 minutes depending on the number of Gems your app uses.
If everything goes smoothly, we're now ready to connect your Puma web server to the Nginx reverse proxy.

you can also install an specific rails version
```
  deploy@<SERVER_IP_ADDRESS>:~$ sudo rm /etc/nginx/sites-enabled/default
  deploy@<SERVER_IP_ADDRESS>:~$ sudo ln -nfs "/home/deploy/apps/<APPNAME>/current/config/nginx.conf" "/etc/nginx/sites-enabled/<APPNAME>"
```
then restart Nginix service:
```
  deploy@<SERVER_IP_ADDRESS>:~$ sudo service nginx restart
```

### After first deploy
This could be a baseline steps for deploy:
```
git add -A
git commit -m "Deploy Message"
git push origin master
cap production deploy
```
